import os, shutil
print("Agregue el dominio a migrar:")
domain = input()
backup_path = "/storage/GetMail/backups/%s/" % domain
destination_path = "/storage/GetMail/vmail/%s/" % domain
print("Agregue el folder de origen: ej. new, cur,tmp, etc")
origin_folder = input()
print("Agregue el folder de destino: ej. cur, new, tmp, etc")
destination_folder = input()
result = os.listdir(backup_path)

def copy_contents(o, d):
	contents = os.listdir(o)
	for element in contents:
		origin = "%s%s" % (o, element)
		destination = "%s%s" % (d, element)
		shutil.move(origin, destination)
	print("%s elementos movidos" % len(contents))

for element in result:
	email_path = "%s%s" % (backup_path, element)
	origin_path = "%s/%s/" % (email_path, origin_folder)
	if os.path.isdir(email_path) and os.path.isdir(origin_path) is True:
		destination = "%s%s/%s/" % (destination_path, element, destination_folder)
		if os.path.isdir(destination):
			copy_contents(origin_path, destination)
			print("%s completado" % element)
			print("----------------------------------------------------------------")
		else:
			print("No se encontro el directorio de destino para %s" % element)
			print("----------------------------------------------------------------")
	else:
		if os.path.isfile(email_path) is False:
			print("No se encontro el directorio de origen %s" % element)
print("Archivos movidos correctamente")
