import os, subprocess
## Importa las variables y la lista de correos a respaldar
from init import email_dirs, domain_path, domain

 ## Crea un archivo de configuracion por cada cuenta de correo, este es el primer
 ##paso antes de correr el script de importacion
for email in email_dirs:
    emailPath = "%s%s" % (domain_path, email)
    if os.path.exists(emailPath):
        string = 'getmail --getmaildir %s --rcfile %s/getmailrc' % (domain_path, emailPath)
        subprocess.call(string, shell=True)
