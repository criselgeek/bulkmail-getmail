from init import email_dirs, config_file, domain, server, domain_path, append, connection_type, verbose, mailboxes, read_all, log_file
import os

for email in email_dirs:
    mailDir = "%s%s" % (domain_path, email)
    if os.path.exists(mailDir):
        cFile = open(config_file)
        newgetmailrc = '%s/getmailrc' % (mailDir)
        ng = open(newgetmailrc, 'w')
        for line in cFile:
            line = line.strip()
            if line.startswith('username'):
                line = 'username = %s@%s' % (email, domain)
            elif line.startswith('password'):
                line = 'password = %s' % (email_dirs[email])
            elif line.startswith('server'):
                line = 'server = %s' % (server)
            elif line.startswith('path'):
                line = 'path = %s/%s' % (mailDir, append)
            elif line.startswith('message_log'):
                line = 'message_log = %s' % (log_file)
            elif line.startswith('xtype'):
                line = 'type = %s' % (connection_type)
            elif line.startswith('mailboxes'):
                line = 'mailboxes = %s' % (mailboxes)
            elif line.startswith('verbose'):
                line = 'verbose = %s' % (verbose)
            elif line.startswith('read_all'):
                line = 'read_all = %s' % (read_all)
            ng.write(line + '\n')
        ng.close()
        cFile.close()
    else:
        print("No existe el directorio del correo para %s" % (email))
