# BulkMail GetMail
Interfaz de respaldo automatizado de multiples cuentas de correo electrónico por medio de __GetMail__ desarrollado en __Python__ para ser utilizado desde la línea de comandos en la consola/terminal. Especialmente diseñado para funcionar en servidores Linux que utilicen Qmail.

## Descarga de responsabilidades
Use este programa bajo su propio riesgo, no me hago responsable por perdida de información, mal funcionamiento del servidor o equipo donde se ejecute o bien cualquier otro problema que se pueda generar del uso de este script.
