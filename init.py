import yaml, logging, os

config = None
with open('config.yml', 'r') as file:
	config = yaml.safe_load(file)
	# Archivo de configuracion para ejecutar getmail desde Qmail
	## Tipo de conexion
	## SimplePOP3Retriever, SimpleIMAPRetriever, SimpleIMAPSSLRetriever, SimplePOP3SSLRetriever
	connection_type = config['connection_type']
	## Mailboxes
	## Se deben probar diferentes opcciones como INBOX.sent, Sent, Sent Mail
	mailboxes = config['mailboxes']
	# Dominio sin www
	domain = config['domain']
	# Ruta dentro de la cual se crearan las carpetas de las direcciones de correo y
	# la carpeta raiz del dominio
	path = config['path']
	# Ruta donde se crearan las carptas de los correos incluyendo la direccion del
	# dominio
	domain_path = '%s%s/' % (path, domain)
	# Ubicacion del archivo de logs general del dominio
	log_file = '%sdebug.log' % (domain_path)
	# Nombre del template de archivo de configuracion de la cuenta
	config_file = 'getmailrc'
	# Directorio dentro del cual estaran las carpetas cur, new y tmp, se debe poner
	# un slash '/' al final. Utilizar Maildir/ para Qmail
	append = config['append']
	# Servidor de entrada del dominio
	#server = 'mail.' + domain
	# Para gmail u otros dominios que no utilizan el nombre de dominio dentro del
	# servidor de entrada
	server = config['server']
	# Verbose
	# 0: solo mostrara mensajes acerca de errores y alertas
	# 1: Mostrara mensajes acerca de borrado y descarga de archivos solamente
	# 2: Mostrara mensajes acerca de todas las acciones
	verbose = config['verbose']
	## Define si se descargan todos los mensajes cada vez que se ejecuta o no
	## true: Descarga todos los mensajes cada vez que se ejecuta no importa si ya
	## fueron descargados
	## false: solo descarga los mensajes nuevos
	read_all = config['read_all']

	# Lista de direcciones de correo electronico sin @dominio.tld
	email_dirs = config['email_dirs']

	# Si no existe el directorio correspondiente al dominio a respaldar se crea el
	# mismo
	if not os.path.exists(domain_path):
		try:
			os.mkdir(domain_path, 0o777)
			log = open(log_file, 'a')
			log.close()
		# En qmail se crea una carpeta para cada cuenta de correo dentro de una carpeta
		# que tiene por nombre el nombre del dominio, estas carpetas de las cuentas de
		# correo se crear solamente con el nombre de usuario si la terminacion
		# @dominio.tld
		except:
			print('No se pudo crear el directorio del dominio %s' %domain_path)
	if os.path.exists(log_file):
		logging.basicConfig(filename=domain_path+'debug.log', filemode='w', format='%(name)s - %(levelname)s - %(message)s', level=logging.DEBUG)
	else:
		print("No existe el archivo debug.log")
