# Guia de desarrolladores

## Funcionalidades
1. Crea un conjunto de carpetas con el nombre del usuario de correo electrónico sin la terminación __@dominio.tld__ dentro de una carpeta con el nombre del dominio, ademas crea dentro de estas carpetas las subcarpetas _cur_, _new_ y _tmp_ como se haria en Qmail. Este es un script que se puede ejecutar opcionalmente cuando se hace un respaldo en una computadora local, por ejemplo.
2. A partir de un template de crea un archivo __getmailrc__ para cada una de las cuentas de correo.
3. Automaticamente descarga los archivos de cada cuenta de correo electrónico a partir de la configuracion en __getmailrc__ ejecutando el script segun se indica en la págian de GetMail.

## Requerimientos
- __Python__
- __GetMail__

## archivos
* __getMailConfig.py__: Configuración general para respaldo de correos, creación de carpetas y creación de archivo getmailrc.
* __createConfig.py__: (_Ejecutable_) Crea el archivo de configuración para cada una de las cuentas de correo.
* __createEmailPaths.py__: (_Ejecutable, Opcional_) Crea los directorios y subdirectorios donde se respaldarán los correos.
__getMailExecute.py__: (_Ejecutable_) Inicia el proceso de respaldo de los correos.

## Configuración
Solo se debe manipular el archivo __getMailerConfig.py__ como se indica a continuación:

```
# Archivo de configuracion para ejecutar getmail desde Qmail
## Tipo de conexion
## SimplePOP3Retriever, SimpleIMAPRetriever, SimpleIMAPSSLRetriever, SimplePOP3SSLRetriever

conexion_type = 'SimpleIMAPRetriever'

## Mailboxes
## Se deben probar diferentes opcciones como INBOX.sent, Sent, Sent Mail

mailboxes = '("Inbox", "Sent",)'

# Dominio sin www

domain = 'domain.tld'

# Ruta dentro de la cual se crearan las carpetas de las direcciones de correo y la carpeta raiz del dominio

ruta = '/path/to/download/message/'

# Ruta donde se crearan las carptas de los correos incluyendo la direccion del dominio

nuevaRuta = ruta + domain + '/'

# Ubicacion del archivo de logs general del dominio

logFile = nuevaRuta + 'getmail.log'

# Nombre del template de archivo de configuracion de la cuenta

configFile = 'getmailrc'

# Directorio dentro del cual estaran las carpetas cur, new y tmp, se debe poner un slash '/' al final

append = ''

# Servidor de entrada del dominio

server = 'mail.' + domain

# Para gmail u otros dominios que no utilizan el nombre de dominio dentro del servidor de entrada

server = 'imap.domain.tld'

## Verbose
# 0: solo mostrará mensajes acerca de errores y alertas
# 1: Mostrará mensajes acerca de borrado y descarga de archivos solamente
# 2: Mostrará mensajes acerca de todas las acciones

verbose = 0

## Define si se descargan todos los mensajes cada vez que se ejecuta o no
# true: Descarga todos los mensajes cada vez que se ejecuta no importa si ya fueron descargados
# false: solo descarga los mensajes nuevos

read_all = false

## Lista de direcciones de correo electronico sin @dominio.tld

eDirs = dict()

eDirs = {
        'email1':'password',
        'email2':'password',
        ...
}


```

## Modo de uso

1. Descargar los archivos de este repositorio en la ubicación de su elección.
2. Configurar el archivo __getMailerConfig.py__ para determinar la ubicación del respaldo, los correos a respaldar y otros parametros necesarios para dicho proceso.
3. Seguir los pasos que se indican abajo dependiendo si es un equipo local o un servidor.

### Maquina local

Se puede ejecutar en una maquina local como usuario normal mediante los siguientes comandos:

__Crear directorios__:

```
$ python createEmailPaths.py
```

__Crear archivos getmailrc__:

```
$ python createConfig.py
```

__Iniciar respaldo__:

```
$ python getMailExecute.py
```

### Servidor con Qmail

Antes de iniciar se recomienda que se creen las cuentas de correo desde el panel de control y luego determinar la ubicación del directorio donde se guardaran los correos en el servidor de forma predeterminada. una vez realizado esto podremos configurar el archivo __getMailerConfig.py__ y seguir estas instrucciones para poder tener los correos disponibles en nuestro webmail la proxima vez que ingresemos.

Para ejecutarlo en un servidor se requiren algunos ajustes para que los correos puedan ser visualizados mediante __webmail__ o bien descargados al cliente de correo de preferencia.

En este caso se debe ejecutar como el usuario __popuser__ (basandose en la configuracion de Plesk 12 sobre Centos 6.8).

__Crear directorios__:

```
$ sudo - popuser sh -c 'python createEmailPaths.py'
```

__Crear archivos getmailrc__:

```
$ sudo - popuser sh -c 'python createConfig.py'
```

__Iniciar respaldo__:

```
$ sudo - popuser sh -c 'python getMailExecute.py'
```

## Referencias

[GetMail](http://pyropus.ca/software/getmail/)
