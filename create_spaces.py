import os, logging
from init import domain_path, domain, config_file, email_dirs, server

# Crea los directorios de los correos donde se respaldaran los archivos y ademas
#  se crean los archivos cur, new, tmp
def crearDirectorios(ePath):
	try:
		if not os.path.exists(ePath):
			os.mkdir(ePath, 0o755)
		curDir = '%scur' % (ePath)
		if not os.path.exists(curDir) and os.path.exists(ePath):
			os.mkdir(curDir, 0o755)

		newDir = '%snew' % (ePath)
		if not os.path.exists(newDir) and os.path.exists(ePath):
			os.mkdir(newDir, 0o755)

		tmpDir = '%stmp' % (ePath)
		if not os.path.exists(tmpDir) and os.path.exists(ePath):
			os.mkdir(tmpDir, 0o755)
	except:
		raise Exception("No se pudo crear uno de los directorios %s" %ePath)

for key in email_dirs:
	ePath = domain_path + key + '/'
	try:
		crearDirectorios(ePath)
		logging.debug("Directorios creados correctamente")
	except Exception as e:
		logging.debug(e)
